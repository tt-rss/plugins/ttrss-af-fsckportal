<?php
class Af_Fsckportal extends Plugin {

	function about() {
		return array(null,
			"Remove feedsportal spamlinks from article content",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	}

	function hook_article_filter($article) {

			$doc = new DOMDocument();

			if (@$doc->loadHTML('<?xml encoding="UTF-8">' . $article["content"])) {
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query('(//img[@src]|//a[@href])');

				foreach ($entries as $entry) {
					if (preg_match("/feedsportal.com/", $entry->getAttribute("src"))) {
						$entry->parentNode->removeChild($entry);
					} else if (preg_match("/feedsportal.com/", $entry->getAttribute("href"))) {
						$entry->parentNode->removeChild($entry);
					}
				}

				$article["content"] = $doc->saveHTML();
		}

		return $article;
	}

	function api_version() {
		return 2;
	}

}
